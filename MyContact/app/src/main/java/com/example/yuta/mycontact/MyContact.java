package com.example.yuta.mycontact;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

// My連絡先アクティビティ
public class MyContact extends Activity{
    // データベース関連
    MyContactDBHelper helper = null;
    SQLiteDatabase dbRead = null;

    // onCreate()メソッド
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // レイアウトファイルのセット
        setContentView(R.layout.activity_my_contact);
        // 新規登録ボタンオブジェクトの生成&取得
        Button newSave = (Button) findViewById(R.id.buttonNewsave);
        // クリックリスナーのセット
        newSave.setOnClickListener(new onNewSaveClickListener());
        // リストビューセットメソッド
        ListViewSet();
        // リストビューを取得
        ListView lv = (ListView)findViewById(R.id.listView);
        // リストビューにリストアイテムクリックリスナーをセット
        lv.setOnItemClickListener(new ListItemClickListener());
    }
    // リスタート時
    @Override
    protected void onRestart(){
        super.onRestart();
        // リストビューセットメソッド
        ListViewSet();
    }

    // リストビューアイテムクリックリスナー
    class ListItemClickListener implements OnItemClickListener{
        // アイテムクリックメソッド
        public void onItemClick(AdapterView<?> parent, View view, int id, long position){
            // 連絡先表示アクティビティクラスのインテントの生成
            Intent intent = new Intent(MyContact.this, MyContactDisp.class);
            // 選択位置で渡す
            intent.putExtra("SELECT", position);
            // アクティビティスタート
            startActivity(intent);
        }
    }

    // クリックリスナー
    class onNewSaveClickListener implements OnClickListener{
        public void onClick(View arg0){
            // インテントの生成
            Intent intent = new Intent(MyContact.this, MyContactInput.class);
            // 次のアクティビティの開始
            startActivity(intent);
        }
    }

    // リストビューセットメソッド
    public void ListViewSet(){
        // ArrayListの作成
        ArrayList<String> alist = new ArrayList<String>();
        // ヘルパークラスのインスタンス生成
        helper = new MyContactDBHelper(this);
        // データベースの取得
        dbRead = helper.getReadableDatabase();
        try {
            Cursor cursor = dbRead.query("MyContactList", ConstDef.columns, null, null, null, null, "name");
            while (cursor.moveToNext()){
                String name = cursor.getString(0);
                alist.add(name);
            }
            cursor.close();

            // アレイアダプタの生成
            ArrayAdapter<String> la = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alist);
            ListView listView = (ListView) findViewById(R.id.listView);
            // リストビューにアレイアダプタをセット
            listView.setAdapter(la);
        }catch (Exception e){
            Toast.makeText(this, "データベースがまだないか，読めません", Toast.LENGTH_LONG).show();
        }
        // DBクローズ
        dbRead.close();;
    }

    // オプションメニュー
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // オプションメニューのアイテム選択時メソッド
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        // 削除メニューが選択されている場合の処理
        if(item.getItemId() == R.id.menu_delete){
            try{
                // ヘルパークラスのインスタンス生成
                helper = new MyContactDBHelper(MyContact.this);
                // データベースの取得
                dbRead = helper.getReadableDatabase();
                // SQL文の作成
                dbRead.execSQL("drop table if exists MyContactList;");
                // DBクローズ
                dbRead.close();
            }catch (Exception e){
                // Toastを表示
                Toast.makeText(this, "削除に失敗しました", Toast.LENGTH_SHORT).show();
            }
            // ArrayListの作成
            ArrayList<String> arrayList = new ArrayList<String>();
            // アレイアダプタの生成
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
            // アレイアダプタのクリア
            arrayAdapter.clear();
            // リストビュー取得
            ListView listView = (ListView) findViewById(R.id.listView);
            // リストビューにアレイアダプタをセット
            listView.setAdapter(arrayAdapter);
        }
        // アプリ情報メニューが選択されている場合の処理
        else if (item.getItemId() == R.id.menu_apliinfo){
            // パッケージマネージャの生成
            PackageManager pkm = getPackageManager();
            // パッケージ情報の確保
            PackageInfo info = null;
            try{
                // パッケージ情報の取得
                info = pkm.getPackageInfo("com.example.yuta.mycontact", PackageManager.GET_INSTRUMENTATION);
            }catch(Exception e){
            }
            // ダイアログで表示
            // アラートダイアログの生成
            AlertDialog.Builder dlog = new AlertDialog.Builder(this);
            // タイトルをstrings.xmlからセット
            dlog.setTitle(R.string.menu_apliinfo);
            // バージョンの表示
            dlog.setMessage("VersionCode: " + String.valueOf(info.versionCode) + " VersionName: " + info.versionName);
            // ボタンの表示
            dlog.setPositiveButton("OK", null);
            // ダイアログの表示
            dlog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}
